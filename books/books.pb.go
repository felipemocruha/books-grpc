// Code generated by protoc-gen-go. DO NOT EDIT.
// source: books.proto

package books

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type BookRequest struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	With                 string   `protobuf:"bytes,3,opt,name=with,proto3" json:"with,omitempty"`
	Completed            bool     `protobuf:"varint,4,opt,name=completed,proto3" json:"completed,omitempty"`
	CreatedAt            int64    `protobuf:"varint,5,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *BookRequest) Reset()         { *m = BookRequest{} }
func (m *BookRequest) String() string { return proto.CompactTextString(m) }
func (*BookRequest) ProtoMessage()    {}
func (*BookRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_01e0dc127ded4184, []int{0}
}

func (m *BookRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BookRequest.Unmarshal(m, b)
}
func (m *BookRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BookRequest.Marshal(b, m, deterministic)
}
func (m *BookRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BookRequest.Merge(m, src)
}
func (m *BookRequest) XXX_Size() int {
	return xxx_messageInfo_BookRequest.Size(m)
}
func (m *BookRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_BookRequest.DiscardUnknown(m)
}

var xxx_messageInfo_BookRequest proto.InternalMessageInfo

func (m *BookRequest) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *BookRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *BookRequest) GetWith() string {
	if m != nil {
		return m.With
	}
	return ""
}

func (m *BookRequest) GetCompleted() bool {
	if m != nil {
		return m.Completed
	}
	return false
}

func (m *BookRequest) GetCreatedAt() int64 {
	if m != nil {
		return m.CreatedAt
	}
	return 0
}

type BookResponse struct {
	Success              bool     `protobuf:"varint,1,opt,name=success,proto3" json:"success,omitempty"`
	Message              string   `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *BookResponse) Reset()         { *m = BookResponse{} }
func (m *BookResponse) String() string { return proto.CompactTextString(m) }
func (*BookResponse) ProtoMessage()    {}
func (*BookResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_01e0dc127ded4184, []int{1}
}

func (m *BookResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BookResponse.Unmarshal(m, b)
}
func (m *BookResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BookResponse.Marshal(b, m, deterministic)
}
func (m *BookResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BookResponse.Merge(m, src)
}
func (m *BookResponse) XXX_Size() int {
	return xxx_messageInfo_BookResponse.Size(m)
}
func (m *BookResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_BookResponse.DiscardUnknown(m)
}

var xxx_messageInfo_BookResponse proto.InternalMessageInfo

func (m *BookResponse) GetSuccess() bool {
	if m != nil {
		return m.Success
	}
	return false
}

func (m *BookResponse) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

type BookFilter struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *BookFilter) Reset()         { *m = BookFilter{} }
func (m *BookFilter) String() string { return proto.CompactTextString(m) }
func (*BookFilter) ProtoMessage()    {}
func (*BookFilter) Descriptor() ([]byte, []int) {
	return fileDescriptor_01e0dc127ded4184, []int{2}
}

func (m *BookFilter) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BookFilter.Unmarshal(m, b)
}
func (m *BookFilter) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BookFilter.Marshal(b, m, deterministic)
}
func (m *BookFilter) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BookFilter.Merge(m, src)
}
func (m *BookFilter) XXX_Size() int {
	return xxx_messageInfo_BookFilter.Size(m)
}
func (m *BookFilter) XXX_DiscardUnknown() {
	xxx_messageInfo_BookFilter.DiscardUnknown(m)
}

var xxx_messageInfo_BookFilter proto.InternalMessageInfo

func (m *BookFilter) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *BookFilter) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func init() {
	proto.RegisterType((*BookRequest)(nil), "books.BookRequest")
	proto.RegisterType((*BookResponse)(nil), "books.BookResponse")
	proto.RegisterType((*BookFilter)(nil), "books.BookFilter")
}

func init() { proto.RegisterFile("books.proto", fileDescriptor_01e0dc127ded4184) }

var fileDescriptor_01e0dc127ded4184 = []byte{
	// 301 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x94, 0x92, 0x41, 0x4f, 0xc2, 0x40,
	0x10, 0x85, 0xd9, 0x02, 0x42, 0x07, 0x43, 0xe2, 0x70, 0xd9, 0x10, 0x4d, 0x48, 0x4f, 0x3d, 0x11,
	0x82, 0x09, 0x1e, 0xbc, 0x28, 0x18, 0xbd, 0x78, 0x6a, 0x62, 0x3c, 0x9a, 0xd2, 0x4e, 0xb4, 0x81,
	0xb2, 0xb5, 0xb3, 0xc4, 0xbb, 0x7f, 0xd5, 0x3f, 0x62, 0x76, 0x97, 0x0a, 0x26, 0x92, 0xd4, 0xdb,
	0xbe, 0xd7, 0xbe, 0x97, 0x6f, 0x66, 0x17, 0x7a, 0x4b, 0xa5, 0x56, 0x3c, 0x2e, 0x4a, 0xa5, 0x15,
	0xb6, 0xad, 0x08, 0x3e, 0x05, 0xf4, 0xe6, 0x4a, 0xad, 0x22, 0x7a, 0xdf, 0x12, 0x6b, 0xec, 0x83,
	0x97, 0xa5, 0x52, 0x8c, 0x44, 0xe8, 0x47, 0x5e, 0x96, 0x22, 0x42, 0x6b, 0x13, 0xe7, 0x24, 0x3d,
	0xeb, 0xd8, 0xb3, 0xf1, 0x3e, 0x32, 0xfd, 0x26, 0x9b, 0xce, 0x33, 0x67, 0x3c, 0x07, 0x3f, 0x51,
	0x79, 0xb1, 0x26, 0x4d, 0xa9, 0x6c, 0x8d, 0x44, 0xd8, 0x8d, 0xf6, 0x06, 0x5e, 0x00, 0x24, 0x25,
	0xc5, 0x9a, 0xd2, 0x97, 0x58, 0xcb, 0xf6, 0x48, 0x84, 0xcd, 0xc8, 0xdf, 0x39, 0xb7, 0x3a, 0x98,
	0xc3, 0xa9, 0x63, 0xe0, 0x42, 0x6d, 0x98, 0x50, 0x42, 0x87, 0xb7, 0x49, 0x42, 0xcc, 0x96, 0xa4,
	0x1b, 0x55, 0xd2, 0x7c, 0xc9, 0x89, 0x39, 0x7e, 0xad, 0x88, 0x2a, 0x19, 0x4c, 0x00, 0x4c, 0xc7,
	0x7d, 0xb6, 0xd6, 0x54, 0xd6, 0x19, 0x63, 0xfa, 0xe5, 0x41, 0xdb, 0x44, 0x18, 0xaf, 0x00, 0x16,
	0x16, 0xc6, 0x48, 0xc4, 0xb1, 0xdb, 0xd3, 0xc1, 0x5a, 0x86, 0x83, 0x5f, 0x9e, 0xc3, 0x0c, 0x1a,
	0x38, 0x03, 0xff, 0x31, 0x63, 0xed, 0x5a, 0xce, 0x0e, 0xfe, 0x71, 0x18, 0xc3, 0x3f, 0xaa, 0x82,
	0xc6, 0x44, 0xe0, 0x14, 0x3a, 0x0f, 0x64, 0x63, 0xb5, 0x53, 0x38, 0x03, 0xb8, 0x23, 0xb3, 0xce,
	0x63, 0xb1, 0x23, 0x8c, 0x37, 0x30, 0x78, 0x2a, 0xd2, 0xdd, 0x70, 0x8b, 0x9f, 0x2b, 0xf9, 0xc7,
	0x94, 0xd7, 0xd0, 0xdf, 0x37, 0x3c, 0x9b, 0xdb, 0xae, 0x1f, 0x5e, 0x9e, 0xd8, 0xe7, 0x76, 0xf9,
	0x1d, 0x00, 0x00, 0xff, 0xff, 0xeb, 0xcb, 0xba, 0x93, 0x7d, 0x02, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// BooksClient is the client API for Books service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type BooksClient interface {
	CreateBook(ctx context.Context, in *BookRequest, opts ...grpc.CallOption) (*BookResponse, error)
	ListBooks(ctx context.Context, in *BookFilter, opts ...grpc.CallOption) (Books_ListBooksClient, error)
	GetBook(ctx context.Context, in *BookFilter, opts ...grpc.CallOption) (*BookRequest, error)
	DeleteBook(ctx context.Context, in *BookFilter, opts ...grpc.CallOption) (*BookResponse, error)
	UpdateBookCompleted(ctx context.Context, in *BookRequest, opts ...grpc.CallOption) (*BookResponse, error)
	UpdateBookWith(ctx context.Context, in *BookRequest, opts ...grpc.CallOption) (*BookResponse, error)
}

type booksClient struct {
	cc *grpc.ClientConn
}

func NewBooksClient(cc *grpc.ClientConn) BooksClient {
	return &booksClient{cc}
}

func (c *booksClient) CreateBook(ctx context.Context, in *BookRequest, opts ...grpc.CallOption) (*BookResponse, error) {
	out := new(BookResponse)
	err := c.cc.Invoke(ctx, "/books.Books/CreateBook", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *booksClient) ListBooks(ctx context.Context, in *BookFilter, opts ...grpc.CallOption) (Books_ListBooksClient, error) {
	stream, err := c.cc.NewStream(ctx, &_Books_serviceDesc.Streams[0], "/books.Books/ListBooks", opts...)
	if err != nil {
		return nil, err
	}
	x := &booksListBooksClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type Books_ListBooksClient interface {
	Recv() (*BookRequest, error)
	grpc.ClientStream
}

type booksListBooksClient struct {
	grpc.ClientStream
}

func (x *booksListBooksClient) Recv() (*BookRequest, error) {
	m := new(BookRequest)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *booksClient) GetBook(ctx context.Context, in *BookFilter, opts ...grpc.CallOption) (*BookRequest, error) {
	out := new(BookRequest)
	err := c.cc.Invoke(ctx, "/books.Books/GetBook", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *booksClient) DeleteBook(ctx context.Context, in *BookFilter, opts ...grpc.CallOption) (*BookResponse, error) {
	out := new(BookResponse)
	err := c.cc.Invoke(ctx, "/books.Books/DeleteBook", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *booksClient) UpdateBookCompleted(ctx context.Context, in *BookRequest, opts ...grpc.CallOption) (*BookResponse, error) {
	out := new(BookResponse)
	err := c.cc.Invoke(ctx, "/books.Books/UpdateBookCompleted", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *booksClient) UpdateBookWith(ctx context.Context, in *BookRequest, opts ...grpc.CallOption) (*BookResponse, error) {
	out := new(BookResponse)
	err := c.cc.Invoke(ctx, "/books.Books/UpdateBookWith", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// BooksServer is the server API for Books service.
type BooksServer interface {
	CreateBook(context.Context, *BookRequest) (*BookResponse, error)
	ListBooks(*BookFilter, Books_ListBooksServer) error
	GetBook(context.Context, *BookFilter) (*BookRequest, error)
	DeleteBook(context.Context, *BookFilter) (*BookResponse, error)
	UpdateBookCompleted(context.Context, *BookRequest) (*BookResponse, error)
	UpdateBookWith(context.Context, *BookRequest) (*BookResponse, error)
}

func RegisterBooksServer(s *grpc.Server, srv BooksServer) {
	s.RegisterService(&_Books_serviceDesc, srv)
}

func _Books_CreateBook_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BookRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BooksServer).CreateBook(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/books.Books/CreateBook",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BooksServer).CreateBook(ctx, req.(*BookRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Books_ListBooks_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(BookFilter)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(BooksServer).ListBooks(m, &booksListBooksServer{stream})
}

type Books_ListBooksServer interface {
	Send(*BookRequest) error
	grpc.ServerStream
}

type booksListBooksServer struct {
	grpc.ServerStream
}

func (x *booksListBooksServer) Send(m *BookRequest) error {
	return x.ServerStream.SendMsg(m)
}

func _Books_GetBook_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BookFilter)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BooksServer).GetBook(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/books.Books/GetBook",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BooksServer).GetBook(ctx, req.(*BookFilter))
	}
	return interceptor(ctx, in, info, handler)
}

func _Books_DeleteBook_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BookFilter)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BooksServer).DeleteBook(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/books.Books/DeleteBook",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BooksServer).DeleteBook(ctx, req.(*BookFilter))
	}
	return interceptor(ctx, in, info, handler)
}

func _Books_UpdateBookCompleted_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BookRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BooksServer).UpdateBookCompleted(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/books.Books/UpdateBookCompleted",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BooksServer).UpdateBookCompleted(ctx, req.(*BookRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Books_UpdateBookWith_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BookRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(BooksServer).UpdateBookWith(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/books.Books/UpdateBookWith",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(BooksServer).UpdateBookWith(ctx, req.(*BookRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _Books_serviceDesc = grpc.ServiceDesc{
	ServiceName: "books.Books",
	HandlerType: (*BooksServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateBook",
			Handler:    _Books_CreateBook_Handler,
		},
		{
			MethodName: "GetBook",
			Handler:    _Books_GetBook_Handler,
		},
		{
			MethodName: "DeleteBook",
			Handler:    _Books_DeleteBook_Handler,
		},
		{
			MethodName: "UpdateBookCompleted",
			Handler:    _Books_UpdateBookCompleted_Handler,
		},
		{
			MethodName: "UpdateBookWith",
			Handler:    _Books_UpdateBookWith_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "ListBooks",
			Handler:       _Books_ListBooks_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "books.proto",
}
