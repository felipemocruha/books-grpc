FROM gcr.io/distroless/static

COPY build/books-server /
CMD books-server
