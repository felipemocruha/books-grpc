postgres:
	docker rm --force proteus-db
	docker run -d --name books-db -p 5432:5432 -e POSTGRES_USER=books -e POSTGRES_PASSWORD=books -e POSTGRES_DB=books postgres:11-alpine

proto:
	protoc -I /usr/local/include -I books books/books.proto --go_out=plugins=grpc:books
	protoc -I /usr/local/include -I books books/books.proto --grpc-web_out=import_style=commonjs,mode=grpcwebtext:ui/src/books-client

server:
	cd server && go build -i -o books-server

cert_dev:
	certstrap --depot-path cert init --common-name "localhost"

compile:
	cd server && CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ../build/books-server .

populate_db:
	cd hack && go build -i -o populate && ./populate

docker:
	@$(MAKE) compile
	docker build -t books-server:debug -f deploy/server.Dockerfile .

fmt:
	cd server && go fmt
	cd client && go fmt

coverage:
	cd server && go test -cover

.PHONY: server proto compile docker populate_db fmt coverage
