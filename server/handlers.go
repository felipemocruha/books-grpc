package main

import (
	api "gitlab.com/felipemocruha/books-grpc/books"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s Service) CreateBook(ctx context.Context, in *api.BookRequest) (*api.BookResponse, error) {
	if len(in.Name) == 0 {
		return nil, status.Errorf(codes.InvalidArgument, "book name cannot be empty")
	}

	if err := s.Store.CreateBook(in.Name); err != nil {
		return nil, status.Errorf(codes.Internal, "failed to create book: %v", err)
	}

	return &api.BookResponse{Success: true, Message: "book created!"}, nil
}

func bookToBookRequest(b *Book) *api.BookRequest {
	return &api.BookRequest{
		Id:        b.ID,
		Name:      b.Name,
		With:      b.With,
		Completed: b.Completed,
		CreatedAt: b.CreatedAt,
	}
}

func (s Service) ListBooks(filter *api.BookFilter, stream api.Books_ListBooksServer) error {
	books, err := s.Store.ListBooks(filter.Name)
	if err != nil {
		return status.Errorf(
			codes.Internal, "failed to list books: %v", err)
	}

	for _, b := range books {
		if err := stream.Send(bookToBookRequest(&b)); err != nil {
			status.Errorf(
				codes.Internal, "failed on books stream send: %v", err)
		}
	}

	return nil
}

func (s Service) GetBook(ctx context.Context, in *api.BookFilter) (*api.BookRequest, error) {
	book, err := s.Store.GetBook(in.Id)
	if err != nil {
		return nil, status.Errorf(
			codes.InvalidArgument, "failed to get book: %v", err)
	}

	return bookToBookRequest(&book), nil
}

func (s Service) DeleteBook(ctx context.Context, in *api.BookFilter) (*api.BookResponse, error) {
	if err := s.Store.DeleteBook(in.Id); err != nil {
		return nil, status.Errorf(
			codes.InvalidArgument, "failed to delete book: %v", err)
	}

	return &api.BookResponse{Success: true, Message: "book deleted!"}, nil
}


func (s Service) UpdateBookCompleted(ctx context.Context, in *api.BookRequest) (*api.BookResponse, error) {
	if err := s.Store.UpdateBookCompleted(in.Completed, in.Id); err != nil {
		return nil, status.Errorf(
			codes.InvalidArgument, "failed to update book completed: %v", err)
	}

	return &api.BookResponse{Success: true, Message: "book updated!"}, nil
}

func (s Service) UpdateBookWith(ctx context.Context, in *api.BookRequest) (*api.BookResponse, error) {
	if err := s.Store.UpdateBookWith(in.With, in.Id); err != nil {
		return nil, status.Errorf(
			codes.InvalidArgument, "failed to update book 'with': %v", err)
	}

	return &api.BookResponse{Success: true, Message: "book updated!"}, nil
}
