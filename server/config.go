package main

import (
	"io/ioutil"
	"os"

	"github.com/golang/glog"
	"gopkg.in/yaml.v2"
)

type DBConfig struct {
	Host     string `yaml:"host"`
	User     string `yaml:"user"`
	Name     string `yaml:"name"`
	Password string `yaml:"password"`
}

type GrpcConfig struct {
	Host     string `yaml:"host"`
	KeyPath  string `yaml:"key-path"`
	CertPath string `yaml:"cert-path"`
}

type Config struct {
	DB     DBConfig     `yaml:"db"`
	Grpc   GrpcConfig   `yaml:"grpc"`
}

func getenv(variable, fallback string) string {
	if val := os.Getenv(variable); len(val) != 0 {
		return val
	} else {
		return fallback
	}
}

func LoadConfig() Config {
	config := Config{}
	configPath := getenv("CONFIG_PATH", "config/config.yaml")

	data, err := ioutil.ReadFile(configPath)
	if err != nil {
		glog.Fatalf("failed to load config: %v", err)
	}

	if err = yaml.Unmarshal(data, &config); err != nil {
		glog.Fatalf("failed to load config: %v", err)
	}

	return config
}
