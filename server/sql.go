package main

//Book queries
const CreateBook = `
INSERT INTO book (id, name, with, completed, created_at)
  VALUES(:id, :name, :with, :completed, :created_at)`

const ListBooks = `
SELECT id, name, with, completed, created_at FROM book ORDER BY name ASC`

const ListBooksFiltered = `
SELECT id, name, with, completed, created_at FROM book
 WHERE name like $1 ORDER BY name ASC`

const GetBookByID = `
SELECT id, name, with, completed, created_at FROM book WHERE id = $1`

const DeleteBookByID = `
DELETE FROM book WHERE id = $1`

const UpdateBookCompleted = `
UPDATE book SET completed = $1 WHERE id = $2`

const UpdateBookWith = `
UPDATE book SET with = $1 WHERE id = $2`
