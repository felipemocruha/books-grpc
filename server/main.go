package main

import (
	"flag"
	"net"
	"net/http"

	"github.com/golang/glog"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	"github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	api "gitlab.com/felipemocruha/books-grpc/books"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

const (
	METRICS_ADDR = "0.0.0.0:8080"
)

type Service struct {
	Store        Store
	Server       *grpc.Server
	Config       *Config
}

func getServerCredentials(config *Config) credentials.TransportCredentials {
	creds, err := credentials.NewServerTLSFromFile(
		config.Grpc.CertPath, config.Grpc.KeyPath)
	if err != nil {
		glog.Fatalf("Failed to setup TLS: %v", err)
	}

	return creds
}

func NewService(config *Config) *Service {
	return &Service{
		Store: Store{DB: CreateDB(config)},
		Server: grpc.NewServer(
			grpc.Creds(getServerCredentials(config)),
			grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
				grpc_prometheus.StreamServerInterceptor,
				grpc_opentracing.StreamServerInterceptor(),
			)),
			grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
				grpc_prometheus.UnaryServerInterceptor,
				grpc_opentracing.UnaryServerInterceptor(),
			)),
		),
		Config:       config,
	}
}

func main() {
	flag.Parse()
	flag.Lookup("logtostderr").Value.Set("true")
	config := LoadConfig()
	service := NewService(&config)

	glog.Infof("Starting server at: %v", config.Grpc.Host)
	listen, err := net.Listen("tcp", config.Grpc.Host)
	if err != nil {
		glog.Fatalf("failed to launch server: %v", err)
	}

	api.RegisterBooksServer(service.Server, service)
	grpc_prometheus.Register(service.Server)
	http.Handle("/metrics", promhttp.Handler())

	go func() {
		glog.Infof("Starting metrics server at: %v/metrics", METRICS_ADDR)
		glog.Error(http.ListenAndServe(METRICS_ADDR, nil))
	}()

	service.Server.Serve(listen)
}
