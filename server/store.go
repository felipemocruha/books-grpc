package main

import (
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/segmentio/ksuid"
)

type Store struct {
	DB *sqlx.DB
}

func (s Store) ListBooks(filter string) (books []Book, err error) {
	if filter != "" {
		if err := s.DB.Select(&books, ListBooksFiltered, filter); err != nil {
			return nil, err
		}
		return books, nil
	}

	if err := s.DB.Select(&books, ListBooks); err != nil {
		return nil, err
	}
	return books, nil
}

func (s Store) GetBook(id string) (book Book, err error) {
	if err := s.DB.Get(&book, GetBookByID, id); err != nil {
		return Book{}, err
	}

	return book, nil
}

func (s Store) CreateBook(name string) error {
	_, err := s.DB.NamedExec(CreateBook, &Book{
		ID:        ksuid.New().String(),
		Name:      name,
		With:      "me",
		Completed: false,
		CreatedAt: time.Now().Unix(),
	})

	return err
}

func (s Store) DeleteBook(id string) error {
	_, err := s.DB.Exec(DeleteBookByID, id)
	return err
}

func (s Store) UpdateBookCompleted(comp bool, id string) error {
	_, err := s.DB.Exec(UpdateBookCompleted, comp, id)
	return err
}

func (s Store) UpdateBookWith(with string, id string) error {
	_, err := s.DB.Exec(UpdateBookWith, with, id)
	return err
}

