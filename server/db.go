package main

import (
	"fmt"

	"github.com/golang/glog"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var schema = `
CREATE TABLE book (
  id text PRIMARY KEY,
  name text,
  with text,
  created_at int
);`

type Book struct {
	ID        string `db:"id"`
	Name      string `db:"name"`
	With      string `db:"with"`
	Completed bool   `db:"completed"`
	CreatedAt int64  `db:"created_at"`
}

func CreateDB(config *Config) *sqlx.DB {
	connStr := fmt.Sprintf(
		"host=%s user=%s dbname=%s sslmode=disable password=%s",
		config.DB.Host,
		config.DB.User,
		config.DB.Name,
		config.DB.Password,
	)

	db, err := sqlx.Connect("postgres", connStr)
	if err != nil {
		glog.Fatalf("failed to create database connection: %v", err)
	}
	db.Exec(schema)

	return db
}
