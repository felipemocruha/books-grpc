/**
 * @fileoverview gRPC-Web generated client stub for books
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.books = require('./books_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.books.BooksClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.books.BooksPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!proto.books.BooksClient} The delegate callback based client
   */
  this.delegateClient_ = new proto.books.BooksClient(
      hostname, credentials, options);

};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.books.BookRequest,
 *   !proto.books.BookResponse>}
 */
const methodInfo_Books_CreateBook = new grpc.web.AbstractClientBase.MethodInfo(
  proto.books.BookResponse,
  /** @param {!proto.books.BookRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.books.BookResponse.deserializeBinary
);


/**
 * @param {!proto.books.BookRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.books.BookResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.books.BookResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.books.BooksClient.prototype.createBook =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/books.Books/CreateBook',
      request,
      metadata,
      methodInfo_Books_CreateBook,
      callback);
};


/**
 * @param {!proto.books.BookRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.books.BookResponse>}
 *     The XHR Node Readable Stream
 */
proto.books.BooksPromiseClient.prototype.createBook =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.createBook(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.books.BookFilter,
 *   !proto.books.BookRequest>}
 */
const methodInfo_Books_ListBooks = new grpc.web.AbstractClientBase.MethodInfo(
  proto.books.BookRequest,
  /** @param {!proto.books.BookFilter} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.books.BookRequest.deserializeBinary
);


/**
 * @param {!proto.books.BookFilter} request The request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.books.BookRequest>}
 *     The XHR Node Readable Stream
 */
proto.books.BooksClient.prototype.listBooks =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/books.Books/ListBooks',
      request,
      metadata,
      methodInfo_Books_ListBooks);
};


/**
 * @param {!proto.books.BookFilter} request The request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.books.BookRequest>}
 *     The XHR Node Readable Stream
 */
proto.books.BooksPromiseClient.prototype.listBooks =
    function(request, metadata) {
  return this.delegateClient_.client_.serverStreaming(this.delegateClient_.hostname_ +
      '/books.Books/ListBooks',
      request,
      metadata,
      methodInfo_Books_ListBooks);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.books.BookFilter,
 *   !proto.books.BookRequest>}
 */
const methodInfo_Books_GetBook = new grpc.web.AbstractClientBase.MethodInfo(
  proto.books.BookRequest,
  /** @param {!proto.books.BookFilter} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.books.BookRequest.deserializeBinary
);


/**
 * @param {!proto.books.BookFilter} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.books.BookRequest)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.books.BookRequest>|undefined}
 *     The XHR Node Readable Stream
 */
proto.books.BooksClient.prototype.getBook =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/books.Books/GetBook',
      request,
      metadata,
      methodInfo_Books_GetBook,
      callback);
};


/**
 * @param {!proto.books.BookFilter} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.books.BookRequest>}
 *     The XHR Node Readable Stream
 */
proto.books.BooksPromiseClient.prototype.getBook =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.getBook(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.books.BookFilter,
 *   !proto.books.BookResponse>}
 */
const methodInfo_Books_DeleteBook = new grpc.web.AbstractClientBase.MethodInfo(
  proto.books.BookResponse,
  /** @param {!proto.books.BookFilter} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.books.BookResponse.deserializeBinary
);


/**
 * @param {!proto.books.BookFilter} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.books.BookResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.books.BookResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.books.BooksClient.prototype.deleteBook =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/books.Books/DeleteBook',
      request,
      metadata,
      methodInfo_Books_DeleteBook,
      callback);
};


/**
 * @param {!proto.books.BookFilter} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.books.BookResponse>}
 *     The XHR Node Readable Stream
 */
proto.books.BooksPromiseClient.prototype.deleteBook =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.deleteBook(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.books.BookRequest,
 *   !proto.books.BookResponse>}
 */
const methodInfo_Books_UpdateBookCompleted = new grpc.web.AbstractClientBase.MethodInfo(
  proto.books.BookResponse,
  /** @param {!proto.books.BookRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.books.BookResponse.deserializeBinary
);


/**
 * @param {!proto.books.BookRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.books.BookResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.books.BookResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.books.BooksClient.prototype.updateBookCompleted =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/books.Books/UpdateBookCompleted',
      request,
      metadata,
      methodInfo_Books_UpdateBookCompleted,
      callback);
};


/**
 * @param {!proto.books.BookRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.books.BookResponse>}
 *     The XHR Node Readable Stream
 */
proto.books.BooksPromiseClient.prototype.updateBookCompleted =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.updateBookCompleted(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.books.BookRequest,
 *   !proto.books.BookResponse>}
 */
const methodInfo_Books_UpdateBookWith = new grpc.web.AbstractClientBase.MethodInfo(
  proto.books.BookResponse,
  /** @param {!proto.books.BookRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.books.BookResponse.deserializeBinary
);


/**
 * @param {!proto.books.BookRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.books.BookResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.books.BookResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.books.BooksClient.prototype.updateBookWith =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/books.Books/UpdateBookWith',
      request,
      metadata,
      methodInfo_Books_UpdateBookWith,
      callback);
};


/**
 * @param {!proto.books.BookRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.books.BookResponse>}
 *     The XHR Node Readable Stream
 */
proto.books.BooksPromiseClient.prototype.updateBookWith =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.updateBookWith(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


module.exports = proto.books;

