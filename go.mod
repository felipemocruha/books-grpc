module gitlab.com/felipemocruha/books-grpc

require (
	cloud.google.com/go v0.36.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/golang/protobuf v1.2.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/opentracing/opentracing-go v1.0.2 // indirect
	github.com/prometheus/client_golang v0.9.2
	github.com/segmentio/ksuid v1.0.2
	golang.org/x/net v0.0.0-20190213061140-3a22650c66bd
	google.golang.org/grpc v1.18.0
	gopkg.in/yaml.v2 v2.2.2
)
